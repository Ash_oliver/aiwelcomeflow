import React, {useState, useEffect} from 'react';
import {
  Button,
  ButtonGroup,
  ProgressHeader,
  TextField,
  GoalCard,
} from '../Components';
import {Page} from '../Layout';
import {SectionContainer, SectionDescription, SectionTitle} from '../Section';
import {get, useModalConfig} from '../Utils';

const GoalsScreen = ({navigation}) => {
  let reloadData = navigation.state.params.reloadData;
  const [goalsList, setGoalsList] = useState(null);
  const [userId, setUserId] = useState(navigation.state.params.userId);
  const modalConfig = useModalConfig();

  const getData = async () => {
    const result = await get('get_client_details', {
      p_cu: userId,
      p_details: 'client_goals',
    });
    setGoalsList(result.data);
  };

  useEffect(() => {
    if (reloadData) {
      getData();
      navigation.setParams({reloadData: false});
    }
  });

  const handleNavigation = page => {
    navigation.navigate(page, {userId: userId, reloadData: true});
  };

  return (
    <Page modalConfig={modalConfig} userId={userId}>
      <ProgressHeader title="Choose your goals" step={2} maxStep={4} />
      <SectionContainer theme="dark">
        <SectionTitle type="dark">Choose your goals</SectionTitle>
        <SectionDescription>
          Add as many as you want. You will be able to fine-tune these goals
          later
        </SectionDescription>
      </SectionContainer>
      <SectionContainer theme="dark">
        {goalsList &&
          goalsList.map((goal, index) => (
            <GoalCard
              goal={goal}
              key={index}
              modalConfig={modalConfig}
              navigation={navigation}
            />
          ))}
        <GoalCard
          isEmpty={true}
          navigation={navigation}
          modalConfig={modalConfig}
        />
      </SectionContainer>
      <SectionContainer theme="dark">
        <ButtonGroup type="spaceBetween">
          <Button
            text="Back"
            type="secondary"
            onPress={() => handleNavigation('AboutYou')}
          />
          <Button
            text="Continue"
            type="primary"
            onPress={() => handleNavigation('Finances')}
          />
        </ButtonGroup>
      </SectionContainer>
    </Page>
  );
};

export default GoalsScreen;
