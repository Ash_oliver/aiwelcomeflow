import React, {useState, useEffect, Fragment} from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import axios from 'axios';
import {GoalTemplateCard} from '../Components';
import {GoalDetailsScreen} from '.';
import {get} from '../Utils';

const ChooseGoalScreen = ({navigation, modalConfig}) => {
  const [goals, setGoals] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getData = async () => {
    const result = await get('get_app_details', {p_details: 'goal_templates'});
    setGoals(result.data);
    setTimeout(() => {
      setIsLoading(false);
    }, 500);
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSelectGoal = id => {
    const goal = goals.find(g => g.id === id);
    modalConfig.openModal({
      component: (
        <GoalDetailsScreen
          goal={goal}
          navigation={navigation}
          closeModal={modalConfig.closeModal}
        />
      ),
      title: 'Choose goal type',
      isFullScreen: true,
    });
  };

  if (isLoading) {
    return (
      <View
        style={{
          backgroundColor: '#fafafa',
          opacity: 0.5,
          flex: 1,
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={{marginBottom: 16}}>
          <ActivityIndicator size="large" color="#0ac" />
        </View>
        <Text>Loading...</Text>
      </View>
    );
  } else {
    return (
      <Fragment>
        {goals &&
          goals.map((item, index) => (
            <GoalTemplateCard
              goal={item}
              key={index}
              selectGoal={handleSelectGoal}
            />
          ))}
      </Fragment>
    );
  }
};

export default ChooseGoalScreen;
