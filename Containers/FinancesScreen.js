import React, {useState, useEffect} from 'react';
import {
  Button,
  ButtonGroup,
  ProgressHeader,
  TextField,
  FinanceItem,
} from '../Components';
import {View, Text} from 'react-native';
import {Page} from '../Layout';
import {SectionContainer, SectionDescription, SectionTitle} from '../Section';
import Svg from './../Svg';
import {get, post, validate} from '../Utils';

const FinancesScreen = ({navigation}) => {
  let reloadData = navigation.state.params.reloadData;
  const [userId, setUserId] = useState(navigation.state.params.userId);
  const [financeItems, setFinanceItems] = useState([]);
  const [validationErrors, setValidationErrors] = useState([]);
  const [data, setData] = useState({});

  const getData = async () => {
    const result = await get('get_client_details', {
      p_cu: userId,
      p_details: 'client_finances',
    });
    setFinanceItems(result.data.items);
    setData(result.data.values);
  };

  useEffect(() => {
    if (reloadData) {
      getData();
      navigation.setParams({reloadData: false});
    }
  });

  const handleChange = (name, value) => {
    setData({...data, [name]: value});
  };

  const saveAndContinue = async () => {
    const fieldMap = [];
    financeItems.map(item => {
      item.textFields.map(field => {
        fieldMap.push({
          name: field.name,
          mapValue: field.mapValue,
          value: data[field.name],
          type: field.type,
        });
      });
    });
    const {errorNames, params, isFullyValid} = validate(fieldMap);
    setValidationErrors(errorNames);
    if (isFullyValid) {
      const result = await post(
        'update_preboarding_financial_async',
        Object.assign(params, {p_cu: userId}),
      );

      navigation.navigate('Email', {userId: userId, reloadData: true});
    }
  };
  return (
    <Page userId={userId}>
      <ProgressHeader title="Snapshot of your finances" step={3} maxStep={4} />
      <SectionContainer theme="dark">
        <SectionTitle type="dark">Snapshot of your finances</SectionTitle>
        <SectionDescription>
          Thanks! Just a few more things before we can build your Wealth Map...
        </SectionDescription>
      </SectionContainer>
      <SectionContainer theme="dark">
        {financeItems &&
          financeItems.map((item, index) => (
            <FinanceItem
              key={index}
              title={item.title}
              icon={item.icon}
              data={data}
              onChange={handleChange}
              description={item.description}
              textFields={item.textFields}
              validationErrors={validationErrors}
            />
          ))}
        <ButtonGroup type="spaceBetween">
          <Button
            text="Back"
            type="secondary"
            onPress={() =>
              navigation.navigate('Goals', {userId: userId, reloadData: true})
            }
          />
          <Button
            text="Continue"
            type="primary"
            onPress={() => saveAndContinue()}
          />
        </ButtonGroup>
      </SectionContainer>
    </Page>
  );
};

export default FinancesScreen;
