import React, {useState, Fragment} from 'react';
import {StyleSheet, View} from 'react-native';
import {post, validate} from '../Utils';
import {
  Button,
  ButtonGroup,
  ProgressHeader,
  TextField,
  RadioGroup,
} from '../Components';
import {Page} from '../Layout';
import {SectionContainer, SectionDescription, SectionTitle} from '../Section';
import Svg from './../Svg';

const GoalDetailsScreen = ({navigation, goal, closeModal}) => {
  const [goalDetails, setGoalDetails] = useState({
    goalId: 0,
    estimatedValue: '',
    yearEnd: '',
    importance: '',
    ...goal,
  });
  const [userId, setUserId] = useState(navigation.state.params.userId);
  const [validationErrors, setValidationErrors] = useState([]);

  const handleChanges = (name, value) => {
    setGoalDetails({...goalDetails, [name]: value});
  };

  const saveGoal = async () => {
    const {errorNames, params, isFullyValid} = validate([
      {
        name: 'name',
        mapValue: 'new_name',
        value: goalDetails.name,
        type: 'text',
        isRequired: true,
      },
      {
        name: 'yearEnd',
        mapValue: 'new_year',
        value: goalDetails.yearEnd,
        type: 'text',
        isRequired: true,
      },
      {
        name: 'estimatedValue',
        mapValue: 'new_amount',
        value: goalDetails.estimatedValue,
        type: 'text',
        isRequired: true,
      },
      {
        name: 'importance',
        mapValue: 'new_priority',
        value: goalDetails.importance,
        type: 'text',
        isRequired: true,
      },
    ]);
    const dataParams = Object.assign(params, {
      p_cu: userId,
      p_goal: goalDetails.goalId,
      new_template: goalDetails.id,
      p_action: 'update',
      p_client: userId,
    });
    setValidationErrors(errorNames);
    if (isFullyValid) {
      const result = await post('update_goal_details_async', dataParams);
      closeModal();
      navigation.navigate('Goals', {userId: userId, reloadData: true});
    }
  };

  const handleClose = () => {
    closeModal();
  };

  return (
    <Fragment>
      <View style={styles.container}>
        <Svg icon={goalDetails.icon} style={styles.icon} />
        <View style={{flex: 1}}>
          <TextField
            value={goalDetails.name}
            onChange={handleChanges}
            name="name"
            isRequired={true}
            errorText="Field cannot be empty"
            isErrorVisible={validationErrors.includes('name')}
          />
        </View>
      </View>

      <TextField
        label="Annual estimated value"
        name="estimatedValue"
        value={String(goalDetails.estimatedValue)}
        onChange={handleChanges}
        type="number-pad"
        isRequired={true}
        errorText="Field cannot be empty"
        isErrorVisible={validationErrors.includes('estimatedValue')}
      />
      <TextField
        label="When (year ending 30 June)?"
        name="yearEnd"
        value={String(goalDetails.yearEnd)}
        onChange={handleChanges}
        type="number-pad"
        isRequired={true}
        errorText="Field cannot be empty"
        isErrorVisible={validationErrors.includes('yearEnd')}
      />

      <RadioGroup
        label="Goal importance?"
        options={[
          {
            value: 1,
            label: 'Non-negotiable',
          },
          {
            value: 2,
            label: 'Great to have',
          },
          {
            value: 3,
            label: 'Nice to have',
          },
        ]}
        checkedValue={goalDetails.importance}
        name="importance"
        onPress={handleChanges}
        isRequired={true}
        errorText="Please select an option"
        isErrorVisible={validationErrors.includes('importance')}
      />
      <ButtonGroup type="spaceBetween">
        <Button text="Back" type="secondary" onPress={() => handleClose()} />
        <Button text="Save" type="primary" onPress={() => saveGoal()} />
      </ButtonGroup>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    borderColor: '#b0b0b0',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    marginBottom: 16,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    height: 64,
    width: 64,
    marginBottom: 20,
    marginRight: 16,
    color: '#455d6e',
  },
});

export default GoalDetailsScreen;
