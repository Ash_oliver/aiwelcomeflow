import React, {useEffect, useState} from 'react';
import {Button, WelcomeStepList} from '../Components';
import {Page} from '../Layout';
import {View} from 'react-native';
import {
  SectionContainer,
  SectionDescription,
  SectionTitle,
  SectionImage,
} from '../Section';
const WealthMap = ({navigation}) => {
  const [userId, setUserId] = useState(navigation.state.params.userId);

  return (
    <Page userId={userId}>
      <SectionContainer theme="dark">
        <SectionTitle type="dark">WORK IN PROGRESS</SectionTitle>
      </SectionContainer>
    </Page>
  );
};

export default WealthMap;
