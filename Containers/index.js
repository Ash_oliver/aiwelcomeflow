import AboutYouScreen from './AboutYouScreen';
import ChooseGoalScreen from './ChooseGoalScreen';
import EmailScreen from './EmailScreen';
import FinancesScreen from './FinancesScreen';
import GoalDetailsScreen from './GoalDetailsScreen';
import GoalsScreen from './GoalsScreen';
import WelcomeScreen from './WelcomeScreen';
import WealthMapScreen from './WealthMapScreen';

export {
  AboutYouScreen,
  ChooseGoalScreen,
  EmailScreen,
  FinancesScreen,
  GoalDetailsScreen,
  GoalsScreen,
  WealthMapScreen,
  WelcomeScreen,
};
