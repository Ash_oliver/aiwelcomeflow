import React, {Fragment} from 'react';
import {Button, WelcomeStepList} from '../Components';
import {Page} from '../Layout';
import {
  SectionContainer,
  SectionDescription,
  SectionTitle,
  SectionImage,
} from '../Section';
import {post} from '../Utils';

const WelcomeScreen = ({navigation}) => {
  const welcomeSteps = [
    {
      icon: 'aboutYou',
      step: 1,
      title: 'About you',
      description: 'Tell us a few things about you',
    },
    {
      icon: 'goals',
      step: 2,
      title: 'Your goals',
      description: 'Choose your life goals',
    },
    {
      icon: 'finances',
      step: 3,
      title: 'Financial snapshot',
      description: 'Add some key financial details',
    },
    {
      icon: 'email',
      step: 4,
      title: 'Enter your email',
      description: 'To access your personal WealthApp',
    },
  ];

  const startWealthMap = async () => {
    if (navigation.state.params && navigation.state.params.userId) {
      navigation.navigate('AboutYou', {
        userId: navigation.state.params.userId,
        reloadData: true,
      });
    } else {
      const result = await post('post_client_details');
      console.log(result);
      navigation.navigate('AboutYou', {userId: result.data.id});
    }
  };

  return (
    <Page>
      <Fragment>
        <SectionImage
          text="Let's have a goals based advice conversation."
          image="welcome"
        />
        <SectionContainer>
          <SectionTitle type="dark">Build your own WealthMap</SectionTitle>
          <SectionDescription>
            Take charge of your future. Our WealthMap helps you explore your
            life goals. It builds a snapshot of your financial foundations to
            help create your life plan
          </SectionDescription>
          <SectionDescription>
            Follow the steps to start your WealthMap journey.
          </SectionDescription>
        </SectionContainer>
        <SectionContainer>
          <WelcomeStepList steps={welcomeSteps} />
        </SectionContainer>
        <SectionContainer>
          <Button
            type="primary"
            text="Build my WealthMap"
            onPress={() => startWealthMap()}
          />
        </SectionContainer>
      </Fragment>
    </Page>
  );
};

export default WelcomeScreen;
