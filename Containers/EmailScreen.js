import React, {useState, useEffect} from 'react';
import {Button, ButtonGroup, ProgressHeader, TextField} from '../Components';
import {Page} from '../Layout';
import {SectionContainer, SectionDescription, SectionTitle} from '../Section';
import {get, post} from '../Utils';

const EmailScreen = ({navigation}) => {
  let reloadData = navigation.state.params.reloadData;
  const [userId, setUserId] = useState(navigation.state.params.userId);
  const [data, setData] = useState({});

  const getData = async () => {
    const result = await get('get_client_details', {
      p_cu: userId,
      p_details: 'client_name',
    });
    setData(result.data);
  };

  useEffect(() => {
    if (reloadData) {
      getData();
      navigation.setParams({reloadData: false});
    }
  });

  const handleChanges = (name, value) => {
    setData({...data, [name]: value});
  };

  const saveAndContinue = async () => {
    //const params = {
    //  p_details: 'save-progress',
    //  new_name: data.firstName,
    //  new_email: data.email,
    //  p_cu: userId,
    //};
    //const result = await post('post_client_details', params);
    navigation.navigate('WealthMap', {userId: userId, reloadData: true});
  };

  const handleNavigation = page => {
    //const data = Object.assign(savedData, {
    //  firstName: data.firstName,
    //  email: data.email,
    //});
    navigation.navigate('WealthMap', {userId: userId});
  };

  return (
    <Page userId={userId}>
      <ProgressHeader title="Save your details" step={4} maxStep={4} />
      <SectionContainer theme="dark">
        <SectionTitle type="dark">Save your details</SectionTitle>
        <SectionDescription>
          Thank you for that! Time for your reward!
        </SectionDescription>
      </SectionContainer>
      <SectionContainer>
        <TextField
          label="First name"
          name="firstName"
          value={data.name}
          onChange={handleChanges}
        />
        <TextField
          label="Email"
          name="email"
          value={data.email !== undefined ? data.email : ''}
          onChange={handleChanges}
          type="email-address"
        />
        <SectionDescription>
          After you click save, we will send you an email confirmation link
          where you can set your password.
        </SectionDescription>

        <ButtonGroup type="spaceBetween">
          <Button
            type="secondary"
            text="Back"
            onPress={() =>
              navigation.navigate('Finances', {
                userId: userId,
                reloadData: true,
              })
            }
          />
          <Button
            text="Save"
            type="primary"
            onPress={() => saveAndContinue()}
          />
        </ButtonGroup>
      </SectionContainer>
      <SectionContainer theme="dark">
        <SectionDescription isLight={true}>
          This advice is general and does not take into account your objectives,
          financial situation or needs. You should consider whether the advice
          is suitable for you and your personal circumstances. (If relevant:
          Before you make any decision about whether to acquire a certain
          product, you should obtain and read the relevant product disclosure
          statement).
        </SectionDescription>
      </SectionContainer>
    </Page>
  );
};

export default EmailScreen;
