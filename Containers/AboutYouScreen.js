import React, {Fragment, useState, useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {get, post, validate, useModalConfig} from '../Utils';
import axios from 'axios';
import {
  Button,
  ButtonGroup,
  ProgressHeader,
  RadioGroup,
  DatePicker,
  TextField,
} from '../Components';
import {Page} from '../Layout';
import {SectionContainer, SectionDescription, SectionTitle} from '../Section';

const AboutYouScreen = ({navigation}) => {
  const [userId, setUserId] = useState(navigation.state.params.userId);
  const [validationErrors, setValidationErrors] = useState([]);
  const [data, setData] = useState({
    dateOfBirth: new Date(),
    partnerDateOfBirth: new Date(),
  });
  const modalConfig = useModalConfig();
  let reloadData = navigation.state.params.reloadData;

  const getData = async () => {
    const result = await get('get_client_details', {
      p_cu: userId,
      p_details: 'about_you',
    });
    const resultData = result.data;
    setData({
      ...resultData,
      dateOfBirth: new Date(resultData.dateOfBirth),
      partnerDateOfBirth: new Date(resultData.partnerDateOfBirth),
    });
  };

  useEffect(() => {
    if (reloadData) {
      getData();
      navigation.setParams({reloadData: false});
    }
  }, []);

  const handleChanges = (name, value) => {
    setData({...data, [name]: value});
  };

  const saveAndContinue = async () => {
    const {errorNames, params, isFullyValid} = validate([
      {
        name: 'individualOrJoint',
        mapValue: 'new_individual_or_couple',
        value: data.individualOrJoint,
        type: 'text',
        isRequired: true,
      },
      {
        name: 'firstName',
        mapValue: 'new_name',
        value: data.firstName,
        type: 'text',
        isRequired: true,
      },
      {
        name: 'dateOfBirth',
        mapValue: 'new_date_of_birth',
        value: data.dateOfBirth,
        type: 'date',
        isRequired: true,
      },
      {
        name: 'gender',
        value: data.gender,
        mapValue: 'new_gender',
        type: 'text',
        isRequired: true,
      },
      {
        name: 'reason',
        value: data.reason,
        mapValue: 'new_what_made_you_seek_advice',
        type: 'text',
        isRequired: true,
      },
      {
        name: 'level',
        value: data.level,
        mapValue: 'new_level_of_financial_knowledge',
        type: 'text',
        isRequired: true,
      },
      {
        name: 'heardOfUs',
        value: data.heardOfUs,
        mapValue: 'new_how_did_you_hear_about_us',
        type: 'text',
        isRequired: true,
      },
      {
        name: 'otherHeardOfUs',
        value: data.otherHeardOfUs,
        mapValue: 'new_how_did_you_hear_about_us_other',
        type: 'text',
        isRequired: data.heardOfUs === 'other',
      },
      {
        name: 'partnerFirstName',
        value: data.partnerFirstName,
        mapValue: 'new_partner_name',
        type: 'text',
        isRequired: data.individualOrJoint === 'couple',
      },
      {
        name: 'partnerDateOfBirth',
        value: data.partnerDateOfBirth,
        mapValue: 'new_partner_date_of_birth',
        type: 'date',
        isRequired: data.individualOrJoint === 'couple',
      },
      {
        name: 'partnerGender',
        value: data.partnerGender,
        mapValue: 'new_partner_gender',
        type: 'text',
        isRequired: data.individualOrJoint === 'couple',
      },
    ]);
    setValidationErrors(errorNames);
    if (isFullyValid) {
      const result = await post(
        'update_preboarding_hero_async',
        Object.assign(params, {p_cu: userId}),
      );
      navigation.navigate('Goals', {userId: userId, reloadData: true});
    }
  };

  return (
    <Page modalConfig={modalConfig}>
      <ProgressHeader title="Opening questions" step={1} maxStep={4} />
      <SectionContainer theme="dark">
        <SectionTitle type="dark">
          Welcome to your financial future!
        </SectionTitle>
        <SectionDescription>
          A few questions to get you started...
        </SectionDescription>
        <SectionDescription>
          Follow the steps to start your WealthMap journey.
        </SectionDescription>
      </SectionContainer>
      <SectionContainer>
        <RadioGroup
          label="Do you want to build an individual or join wealth map?"
          errorText="Please select an option"
          isErrorVisible={validationErrors.includes('individualOrJoint')}
          hasSpacing={true}
          isLabelLarge={true}
          options={[
            {
              value: 'individual',
              label: 'Individual',
            },
            {
              value: 'couple',
              label: 'Joint',
            },
          ]}
          checkedValue={data.individualOrJoint}
          onPress={handleChanges}
          name="individualOrJoint"
          isRequired={true}
        />
        <TextField
          label="What is your first name?"
          errorText="Field cannot be empty"
          isErrorVisible={validationErrors.includes('firstName')}
          hasSpacing={true}
          value={data.firstName}
          onChange={handleChanges}
          isLabelLarge={true}
          name="firstName"
          isRequired={true}
        />
        <DatePicker
          label="What is your date of birth?"
          isLabelLarge={true}
          hasSpacing={true}
          name="dateOfBirth"
          selectedValue={data.dateOfBirth}
          onChange={handleChanges}
          onClick={modalConfig.openModal}
          isRequired={true}
        />
        <RadioGroup
          label="What is your gender?"
          isLabelLarge={true}
          hasSpacing={true}
          options={[
            {
              value: '1',
              label: 'Male',
            },
            {
              value: '2',
              label: 'Female',
            },
          ]}
          checkedValue={data.gender}
          onPress={handleChanges}
          name="gender"
          errorText="Please select an option"
          isErrorVisible={validationErrors.includes('gender')}
          isRequired={true}
        />
        {data.individualOrJoint === 'couple' ? (
          <Fragment>
            <TextField
              label="What is your partner's first name?"
              hasSpacing={true}
              value={data.partnerFirstName}
              onChange={handleChanges}
              isLabelLarge={true}
              name="partnerFirstName"
              errorText="Field cannot be empty"
              isErrorVisible={validationErrors.includes('partnerFirstName')}
              isRequired={true}
            />
            <DatePicker
              label="What is your partner's date of birth?"
              isLabelLarge={true}
              hasSpacing={true}
              name="partnerDateOfBirth"
              selectedValue={data.partnerDateOfBirth}
              onChange={handleChanges}
              onClick={modalConfig.openModal}
            />
            <RadioGroup
              label="What is your partner's gender?"
              isLabelLarge={true}
              hasSpacing={true}
              options={[
                {
                  value: '1',
                  label: 'Male',
                },
                {
                  value: '2',
                  label: 'Female',
                },
              ]}
              checkedValue={data.partnerGender}
              onPress={handleChanges}
              name="partnerGender"
              errorText="Please select an option"
              isErrorVisible={validationErrors.includes('partnerGender')}
              isRequired={true}
            />
          </Fragment>
        ) : null}
        <RadioGroup
          label="What made you decide to seek financial advice?"
          isLabelLarge={true}
          hasSpacing={true}
          options={[
            {
              value: 'need_step_by_step_plan',
              label:
                'I need a definitive step by step action plan that I can rely on, so that I can have the things I need in my life',
            },
            {
              value: 'need_a_proven_system',
              label:
                'I enjoy learning and want to have a proven system that allows me to save time',
            },
            {
              value: 'need_to_provide_for_family',
              label:
                'I need to be able to provide for my family or others and ensure their needs are provided for',
            },
            {
              value: 'looking_for_adviser_to_grow_portfolio',
              label:
                'I am looking for new and different ways to grow my financial portfolio. I am seeking an adviser that thinks outside of the box',
            },
          ]}
          checkedValue={data.reason}
          onPress={handleChanges}
          name="reason"
          errorText="Please select an option"
          isErrorVisible={validationErrors.includes('reason')}
          isRequired={true}
        />
        <RadioGroup
          label="How would you describe your level of financial knowledge?"
          isLabelLarge={true}
          hasSpacing={true}
          options={[
            {
              value: 'beginner',
              label: 'I am a beginner',
            },
            {
              value: 'confident',
              label:
                'I am able to manage finances confidently but there is always room for improvement',
            },
            {
              value: 'knowledgeable',
              label:
                'I know what I am doing but I am looking for higher performance from my investments',
            },
            {
              value: 'not-sure',
              label: 'I do not know',
            },
          ]}
          checkedValue={data.level}
          onPress={handleChanges}
          name="level"
          errorText="Please select an option"
          isErrorVisible={validationErrors.includes('level')}
          isRequired={true}
        />

        <RadioGroup
          label="How did you hear about us?"
          isLabelLarge={true}
          hasSpacing={true}
          options={[
            {
              value: 'google',
              label: 'Google',
            },
            {
              value: 'referral',
              label: 'Referral',
            },
            {
              value: 'facebook',
              label: 'Facebook',
            },
            {
              value: 'linkedIn',
              label: 'LinkedIn',
            },
            {
              value: 'event',
              label: 'Event',
            },
            {
              value: 'other',
              label: 'Other',
            },
          ]}
          checkedValue={data.heardOfUs}
          onPress={handleChanges}
          name="heardOfUs"
          errorText="Please select an option"
          isErrorVisible={validationErrors.includes('heardOfUs')}
          isRequired={true}
        />

        {data.heardOfUs === 'other' ? (
          <TextField
            label="Please specify"
            hasSpacing={true}
            value={data.otherHeardOfUs}
            onChange={handleChanges}
            isLabelLarge={true}
            name="otherHeardOfUs"
            errorText="Field cannot be empty"
            isErrorVisible={validationErrors.includes('otherHeardOfUs')}
            isRequired={true}
          />
        ) : null}

        <ButtonGroup type="spaceBetween">
          <Button
            text="Back"
            type="secondary"
            onPress={() => navigation.navigate('Welcome', {userId: userId})}
          />
          <Button
            text="Continue"
            type="primary"
            onPress={() => saveAndContinue()}
          />
        </ButtonGroup>
      </SectionContainer>
    </Page>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});

export default AboutYouScreen;
