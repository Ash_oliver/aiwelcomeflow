import Logo from './logo.svg';
import AboutYou from './aboutYou.svg';
import Goals from './goals.svg';
import Finances from './finances.svg';
import Email from './email.svg';
import Caret from './caret.svg';
import Close from './close.svg';
import Add from './add.svg';
import Edit from './edit.svg';
import Calendar from './calendar.svg';
import Star from './star.svg';
import User from './user.svg';
import Income from './income.svg';
import Purchase from './purchase.svg';
import Superannuation from './superannuation.svg';
import Save from './save.svg';
import Debt from './debt.svg';
import House from './house.svg';
import Shares from './shares.svg';
import InvestmentProperty from './investmentProperty.svg';
import Travel from './travel.svg';
import Adventure from './adventure.svg';
import Sabbatical from './sabbatical.svg';
import Wealth from './wealth.svg';
import Home from './home.svg';
import SmallBusiness from './smallBusiness.svg';
import Degree from './degree.svg';
import Education from './education.svg';
import Family from './family.svg';
import Support from './support.svg';
import Retire from './retire.svg';
import Wedding from './wedding.svg';

export {
  Logo,
  AboutYou,
  Goals,
  Finances,
  Email,
  Caret,
  Close,
  Add,
  Edit,
  Calendar,
  Star,
  User,
  Income,
  Purchase,
  Superannuation,
  Save,
  Debt,
  House,
  Shares,
  InvestmentProperty,
  Travel,
  Adventure,
  Sabbatical,
  Wealth,
  Home,
  SmallBusiness,
  Degree,
  Education,
  Family,
  Support,
  Retire,
  Wedding,
};
