import styles from './FooterStyles';
import React from 'react';
import {View, Text, Linking} from 'react-native';
import Svg from '../../Svg';

const Footer = () => {
  const pages = [
    {
      preLinkText: 'Powered by',
      link: 'Advice Intelligence',
      url: 'https://www.adviceintelligence.com',
      isAppOnly: false,
    },
    {
      preLinkText: '|',
      link: 'Release Notes',
      url: '/release_notes',
      isAppOnly: true,
    },
    {
      preLinkText: '|',
      link: 'Privacy Policy',
      url: 'https://www.adviceintelligence.com/privacy-policy',
      isAppOnly: true,
    },
    {
      preLinkText: '|',
      link: 'Terms of use',
      url: 'https://www.adviceintelligence.com/terms-of-use',
      isAppOnly: true,
    },
    {
      preLinkText: '|',
      link: 'V1.190827.0743',
      isAppOnly: false,
    },
  ];

  return (
    <View style={styles.Footer}>
      {pages.map((item, index) => [
        !item.isAppOnly ? (
          <Text style={styles.FooterText} key={index}>
            {' '}
            {item.preLinkText}{' '}
            {item.url ? (
              <Text
                style={styles.FooterLink}
                onPress={() => Linking.openURL(item.url)}>
                {item.link}{' '}
              </Text>
            ) : (
              <Text style={styles.FooterText}>{item.link}</Text>
            )}
          </Text>
        ) : null,
      ])}
    </View>
  );
};

export default Footer;
