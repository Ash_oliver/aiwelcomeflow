import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  Footer: {
    backgroundColor: '#f8f8f8',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 32,
    paddingBottom: 80,
  },
  FooterText: {
    color: '#808080',
  },
  FooterLink: {
    color: '#3580d8',
  },
});
