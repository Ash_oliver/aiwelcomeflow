import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  Footer: {
    backgroundColor: '#f8f8f8',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 32,
    paddingBottom: 80,
  },
  FooterText: {
    color: '#808080',
  },
  FooterLink: {
    color: '#3580d8',
  },
  smallModal: {
    marginTop: 500,
    paddingBottom: 200,
    height: 450,
    shadowColor: '#808080',
    shadowOffset: {width: 0, height: -3},
    shadowRadius: 5,
    shadowOpacity: 0.8,
  },
});
