import styles from './PageStyles';
import React, {Fragment} from 'react';
import {
  SafeAreaView,
  ScrollView,
  Modal,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {ModalContainer} from './../../Components';
import {Header, Footer} from './../index';
import Svg from '../../Svg';

const Page = ({children, modalConfig, userId}) => {
  const config = modalConfig || {
    isVisible: false,
  };

  return (
    <Fragment>
      <SafeAreaView>
        <Header userId={userId} />
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View style={{flex: 1}}>{children}</View>
          <Footer />
        </ScrollView>
        {config.isVisible ? <ModalContainer modalConfig={config} /> : null}
      </SafeAreaView>
    </Fragment>
  );
};

export default Page;
