import styles from './HeaderStyles';
import helpers from '../../Helpers';
import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import Svg from '../../Svg';
import {get} from '../../Utils';

const Header = ({userId}) => {
  const [name, setName] = useState('');
  const getData = async () => {
    const result = await get('get_client_details', {
      p_cu: userId,
      p_details: 'client_name',
    });
    setName(result.data.name);
  };

  useEffect(() => {
    if (userId) {
      getData();
    }
  }, []);

  return (
    <View style={styles.header}>
      <Svg icon="logo" style={styles.headerLogo} />
      {name !== '' && (
        <View style={styles.container}>
          <View style={styles.iconContainer}>
            <Svg icon="user" style={styles.icon} />
          </View>
          <Text>{name}</Text>
        </View>
      )}
    </View>
  );
};

export default Header;
