import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  header: {
    alignItems: 'center',
    backgroundColor: '#0ac',
    flexDirection: 'row',
    height: 64,
    justifyContent: 'space-between',
    paddingLeft: 20,
    paddingRight: 8,
  },
  headerLogo: {
    flexDirection: 'row',
    alignItems: 'center',
    color: 'white',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {
    padding: 8,
    borderRadius: 50,
    backgroundColor: '#rgba(0, 0, 0, 0.3)',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 8,
  },
  icon: {
    height: 20,
    width: 20,
    color: '#fff',
  },
});
