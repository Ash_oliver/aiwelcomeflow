import React, {useEffect, useState} from 'react';
import {View, Animated} from 'react-native';

const SlideUp = ({children}) => {
  const [fadeIn, setFadeIn] = useState(new Animated.Value(0));
  const [slideUp, setSlideUp] = useState(new Animated.Value(5));

  useEffect(() => {
    Animated.timing(fadeIn, {
      toValue: 1,
      duration: 300,
    }).start();

    Animated.timing(slideUp, {
      toValue: 0,
      duration: 300,
    }).start();
  });

  return (
    <Animated.View
      style={{
        opacity: fadeIn,
        transform: [
          {
            translateY: slideUp,
          },
        ],
      }}>
      {children}
    </Animated.View>
  );
};

export default SlideUp;
