import React, {useEffect, useState} from 'react';
import {View, Animated} from 'react-native';

const ProgressBar = ({children, value, style}) => {
  const [progress, setProgress] = useState(new Animated.Value(0));

  useEffect(() => {
    Animated.timing(progress, {
      toValue: value,
      duration: 500,
    }).start();
  });

  const widthInterpolated = progress.interpolate({
    inputRange: [0, 1],
    outputRange: ['0%', '100%'],
    extrapolate: 'clamp',
  });

  return (
    <Animated.View
      style={[
        style,
        {
          width: widthInterpolated,
        },
      ]}>
      {children}
    </Animated.View>
  );
};

export default ProgressBar;
