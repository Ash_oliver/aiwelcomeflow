import React from 'react';
import {
  Logo,
  AboutYou,
  Goals,
  Finances,
  Email,
  Caret,
  Close,
  Add,
  Edit,
  Calendar,
  Star,
  User,
  Income,
  Purchase,
  Superannuation,
  Save,
  Debt,
  House,
  Shares,
  InvestmentProperty,
  Travel,
  Adventure,
  Sabbatical,
  Wealth,
  Home,
  SmallBusiness,
  Degree,
  Education,
  Family,
  Support,
  Retire,
  Wedding,
} from './../Icons';

const Svg = ({icon, style}) => {
  const baseStyles = {
    height: 16,
    width: 16,
    color: '#383838',
  };

  const icons = {
    logo: <Logo style={[baseStyles, style]} />,
    aboutYou: <AboutYou style={[baseStyles, style]} />,
    goals: <Goals style={[baseStyles, style]} />,
    finances: <Finances style={[baseStyles, style]} />,
    email: <Email style={[baseStyles, style]} />,
    caret: <Caret style={[baseStyles, style]} />,
    close: <Close style={[baseStyles, style]} />,
    add: <Add style={[baseStyles, style]} />,
    edit: <Edit style={[baseStyles, style]} />,
    calendar: <Calendar style={[baseStyles, style]} />,
    star: <Star style={[baseStyles, style]} />,
    user: <User style={[baseStyles, style]} />,
    income: <Income style={[baseStyles, style]} />,
    purchase: <Purchase style={[baseStyles, style]} />,
    superannuation: <Superannuation style={[baseStyles, style]} />,
    save: <Save style={[baseStyles, style]} />,
    debt: <Debt style={[baseStyles, style]} />,
    house: <House style={[baseStyles, style]} />,
    shares: <Shares style={[baseStyles, style]} />,
    investmentProperty: <Shares style={[baseStyles, style]} />,
    travel: <Travel style={[baseStyles, style]} />,
    adventure: <Adventure style={[baseStyles, style]} />,
    sabbatical: <Sabbatical style={[baseStyles, style]} />,
    wealth: <Wealth style={[baseStyles, style]} />,
    home: <Home style={[baseStyles, style]} />,
    smallBusiness: <SmallBusiness style={[baseStyles, style]} />,
    degree: <Degree style={[baseStyles, style]} />,
    education: <Education style={[baseStyles, style]} />,
    family: <Family style={[baseStyles, style]} />,
    support: <Support style={[baseStyles, style]} />,
    retire: <Retire style={[baseStyles, style]} />,
    wedding: <Wedding style={[baseStyles, style]} />,
  };

  return icons[icon];
};

export default Svg;
