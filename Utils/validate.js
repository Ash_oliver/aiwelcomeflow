import {formatDate} from '.';

const formattedValueMap = {
  date: value => formatDate(value, 'dd-mon-yyyy'),
  text: value => String(value),
  number: value => Number(value),
};

const isValueValid = {
  date: value => true,
  text: value => true,
  email: value => true,
  phone: value => true,
  number: value => true,
};

const isValueEmpty = value =>
  value === undefined || value === '' || value === null;

const validate = array => {
  const errorNames = [];
  const params = {};
  let isFullyValid = true;
  array.forEach(item => {
    const isEmpty = isValueEmpty(item.value);
    const valueIsValid = isValueValid[item.type](item.value);
    if (
      (item.isRequired && isEmpty) ||
      (item.isRequired && !valueIsValid) ||
      (!isEmpty && !valueIsValid)
    ) {
      isFullyValid = false;
      errorNames.push(item.name);
    }
    if (!isEmpty && isValueValid)
      params[item.mapValue] = formattedValueMap[item.type](item.value);
  });
  return {errorNames, params, isFullyValid};
};

export default validate;
