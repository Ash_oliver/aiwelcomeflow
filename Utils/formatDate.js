const months = Object.freeze([
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
]);

const formatDate = (date, format) => {
  const formatMap = {
    'dd-mon-yyyy': `${date.getDate()}-${
      months[date.getMonth()]
    }-${date.getFullYear()}`,
    'dd/mon/yyyy': `${date.getDate()} ${
      months[date.getMonth()]
    } ${date.getFullYear()}`,
  };
  return formatMap[format];
};

export default formatDate;
