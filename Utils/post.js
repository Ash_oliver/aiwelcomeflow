import React from 'react';
import axios from 'axios';

const convertToQueryString = object => {
  let queryString = '';
  for (const [key, value] of Object.entries(object)) {
    queryString = `${queryString}&${key}=${value}`;
  }
  return queryString;
};

const post = async (url, params) => {
  const queryString = params ? `?${convertToQueryString(params)}` : '';
  const result = await axios
    .post(`http://localhost:8080/${url}${queryString}`)
    .then(function(response) {
      console.log(response.data);
      return response.data;
    })
    .catch(function(error) {
      console.log(error);
      return error;
    });

  return result;
};
export default post;
