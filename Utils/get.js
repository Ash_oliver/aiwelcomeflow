import React from 'react';
import axios from 'axios';

const get = async (url, params) => {
  const result = await axios
    .get(`http://localhost:8080/${url}`, {
      params,
    })
    .then(response => {
      console.log(response.data);
      return response.data;
    })
    .catch(error => {
      console.log(error);
      return error;
    });
  return result;
};
export default get;
