import React, {useState, useEffect} from 'react';

const useModalConfig = () => {
  const [config, setConfig] = useState({
    isVisible: false,
    component: null,
    title: null,
    isFullScreen: false,
    isLoading: true,
    openModal: ({component, title, isFullScreen}) =>
      setConfig({
        ...config,
        component: component,
        title: title,
        isVisible: true,
        isFullScreen: isFullScreen,
      }),
    closeModal: () => setConfig({...config, component: null, isVisible: false}),
  });

  return config;
};

export default useModalConfig;
