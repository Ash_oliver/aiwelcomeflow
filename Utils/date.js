const months = Object.freeze([
  {
    label: 'Jan',
    value: 'Jan',
  },
  {
    label: 'Feb',
    value: 'Feb',
  },
  {
    label: 'Mar',
    value: 'Mar',
  },
  {
    label: 'Apr',
    value: 'Apr',
  },
  {
    label: 'May',
    value: 'May',
  },
  {
    label: 'Jun',
    value: 'Jun',
  },
  {
    label: 'Jul',
    value: 'Jul',
  },
  {
    label: 'Aug',
    value: 'Aug',
  },
  {
    label: 'Sep',
    value: 'Sep',
  },
  {
    label: 'Oct',
    value: 'Oct',
  },
  {
    label: 'Nov',
    value: 'Nov',
  },
  {
    label: 'Dec',
    value: 'Dec',
  },
]);

const getYears = () => {
  const maxYear = new Date().getFullYear();
  const minYear = maxYear - 100;
  const years = [];
  for (var i = maxYear; i >= minYear; i--) {
    years.push({
      label: i,
      value: i,
    });
  }
  return years;
};

const years = Object.freeze(getYears());

const getDays = () => {
  const days = [];
  for (var i = 1; i <= 31; i++) {
    days.push({
      label: i,
      value: i,
    });
  }
  return days;
};

const days = Object.freeze(getDays());

export {months, years, days};
