/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';

import {
  WelcomeScreen,
  AboutYouScreen,
  GoalsScreen,
  FinancesScreen,
  EmailScreen,
  GoalDetailsScreen,
  WealthMapScreen,
} from './Containers';

const MainNavigator = createStackNavigator(
  {
    Welcome: WelcomeScreen,
    AboutYou: AboutYouScreen,
    Goals: GoalsScreen,
    Finances: FinancesScreen,
    Email: EmailScreen,
    GoalDetails: GoalDetailsScreen,
    WealthMap: WealthMapScreen,
  },
  {
    initialRouteName: 'Welcome',
    headerMode: 'none',
  },
);

const App = createAppContainer(MainNavigator);

export default App;
