import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    marginBottom: 32,
  },
  radio: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
  radioButton: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#3580d8',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 16,
  },
  checkedRadio: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: '#3580d8',
  },
  radioLabel: {
    fontSize: 16,
    color: '#383838',
    flex: 1,
    flexWrap: 'wrap',
  },
});
