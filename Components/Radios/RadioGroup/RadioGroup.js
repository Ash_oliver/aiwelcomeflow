import styles from '../RadioStyles';
import React from 'react';
import {View, Text} from 'react-native';
import {RadioButton} from './../index';
import {Label} from './../../index';

const RadioGroup = ({
  options,
  label,
  checkedValue,
  onPress,
  isLabelLarge,
  hasSpacing,
  name,
  errorText,
  isErrorVisible,
  isRequired,
}) => {
  return (
    <View style={styles.container}>
      <Label
        text={label}
        isLarge={isLabelLarge}
        hasSpacing={hasSpacing}
        isRequired={isRequired}
      />
      {options.map((item, index) => (
        <RadioButton
          name={name}
          key={item.value}
          item={item}
          isChecked={checkedValue === item.value}
          onPress={onPress}
        />
      ))}
      {isErrorVisible && <Text style={{color: 'red'}}>{errorText}</Text>}
    </View>
  );
};

export default RadioGroup;
