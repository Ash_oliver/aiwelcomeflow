import styles from '../RadioStyles';
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

const RadioButton = ({item, isChecked, onPress, name}) => {
  const handlePress = value => {
    onPress(name, value);
  };
  return (
    <TouchableOpacity
      style={styles.radio}
      activeOpacity={1}
      onPress={() => handlePress(item.value)}>
      <View style={styles.radioButton}>
        {isChecked && <View style={styles.checkedRadio} />}
      </View>
      <Text style={styles.radioLabel}>{item.label}</Text>
    </TouchableOpacity>
  );
};

export default RadioButton;
