import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  welcomeStep: {
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 16,
  },
  icon: {
    color: '#455d6e',
    height: 64,
    width: 64,
  },
  step: {
    color: '#455d6e',
    fontSize: 32,
    fontWeight: '600',
    marginTop: 4,
    marginBottom: 4,
  },
  title: {
    color: '#455d6e',
    fontWeight: '600',
    fontSize: 18,
    marginBottom: 8,
  },
  description: {
    color: '#455d6e',
    fontSize: 16,
    marginBottom: 8,
  },
});
