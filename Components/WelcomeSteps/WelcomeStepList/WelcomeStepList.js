import styles from '../WelcomeStepStyles';
import React from 'react';
import {View} from 'react-native';
import {WelcomeStep} from './../index';

const WelcomeStepList = ({steps}) => {
  return (
    <View style={styles.welcomeStep}>
      {steps.map((item, index) => (
        <WelcomeStep
          key={index}
          icon={item.icon}
          step={item.step}
          title={item.title}
          description={item.description}
        />
      ))}
    </View>
  );
};

export default WelcomeStepList;
