import styles from '../WelcomeStepStyles';
import React from 'react';
import {View, Text} from 'react-native';
import Svg from './../../../Svg';

const WelcomeStep = ({icon, step, title, description}) => {
  return (
    <View style={styles.welcomeStep}>
      <Svg icon={icon} style={styles.icon} />
      <Text style={styles.step}>{step}</Text>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.description}>{description}</Text>
    </View>
  );
};

export default WelcomeStep;
