import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: '#f8f8f8',
    height: 12,
    position: 'relative',
  },
  slim: {
    height: 4,
  },
  sparkline: {
    backgroundColor: '#0ac',
    bottom: 0,
    left: 0,
    position: 'absolute',
    top: 0,
  },
});
