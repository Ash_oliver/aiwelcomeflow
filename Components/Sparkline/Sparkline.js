import styles from './SparklineStyles';
import React from 'react';
import {View} from 'react-native';
import {ProgressBar} from './../../Transitions';

const Sparkline = ({isSlim, step, maxStep}) => {
  const value = (step / maxStep).toFixed(2);
  const slimStyles = isSlim ? styles.slim : {};
  return (
    <View style={[styles.container, slimStyles]}>
      <ProgressBar value={value} style={styles.sparkline} />
    </View>
  );
};

export default Sparkline;
