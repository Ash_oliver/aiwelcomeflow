import styles from './ButtonStyles';
import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

const toTitleCase = string =>
  string.substr(0, 1).toUpperCase() + string.substr(1);

const Button = ({type, text, fullWidth, onPress}) => {
  const textType = `text${toTitleCase(type)}`;
  const isFullWidth = fullWidth ? styles.fullWidth : {};
  return (
    <TouchableOpacity
      style={[styles.button, styles[type], isFullWidth]}
      onPress={onPress}>
      <Text style={[styles.text, styles[textType]]}>{text}</Text>
    </TouchableOpacity>
  );
};

export default Button;
