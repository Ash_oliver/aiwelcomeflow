import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  button: {
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 50,
    fontSize: 16,
    height: 40,
    width: 'auto',
    paddingHorizontal: 16,
  },
  primary: {
    backgroundColor: '#3580d8',
  },
  secondary: {
    borderColor: '#3580d8',
  },
  fullWidth: {
    alignSelf: 'auto',
  },
  text: {
    fontSize: 16,
  },
  textPrimary: {
    color: 'white',
  },
  textSecondary: {
    color: '#3580d8',
  },
  textTertiary: {
    color: '#3580d8',
  },
});
