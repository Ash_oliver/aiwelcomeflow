import styles from './ModalStyles';
import React from 'react';
import {View, Text, Modal, SafeAreaView, ScrollView} from 'react-native';
import {ButtonIcon} from '../index';

const FullScreenModal = ({modalConfig}) => {
  return (
    <Modal
      animationType="slide"
      visible={modalConfig.isVisible}
      presentationStyle="overFullScreen">
      <View
        style={{
          flex: 1,
          marginTop: 22,
        }}>
        <View style={{flex: 1}}>
          <SafeAreaView>
            <View style={styles.header}>
              <Text style={styles.title}>{modalConfig.title}</Text>
              <ButtonIcon
                icon="close"
                style={styles.closeIcon}
                onPress={modalConfig.closeModal}
              />
            </View>
            <ScrollView contentInsetAdjustmentBehavior="automatic">
              <View style={styles.container}>{modalConfig.component}</View>
            </ScrollView>
          </SafeAreaView>
        </View>
      </View>
    </Modal>
  );
};

export default FullScreenModal;
