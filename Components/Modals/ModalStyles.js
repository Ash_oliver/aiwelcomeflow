import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  header: {
    padding: 16,
    borderColor: '#b0b0b0',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  container: {
    paddingHorizontal: 16,
    paddingVertical: 32,
    flex: 1,
  },
  title: {
    fontWeight: '600',
    fontSize: 18,
    color: '#383838',
  },
  closeIcon: {
    height: 24,
    width: 24,
  },
  smallModal: {
    marginTop: 500,
    paddingBottom: 200,
    height: 450,
    shadowColor: '#808080',
    shadowOffset: {width: 0, height: -3},
    shadowRadius: 5,
    shadowOpacity: 0.8,
  },
});
