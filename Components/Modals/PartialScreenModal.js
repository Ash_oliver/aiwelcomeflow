import styles from './ModalStyles';
import React from 'react';
import {View, Modal, Text, TouchableOpacity} from 'react-native';

const ButtonGroup = ({modalConfig}) => {
  const handleClose = () => {
    modalConfig.closeModal();
  };

  return (
    <Modal
      transparent={true}
      animationType="slide"
      visible={modalConfig.isVisible}
      presentationStyle="overFullScreen">
      <View
        style={{
          flex: 1,
          marginTop: 22,
        }}>
        <View style={styles.smallModal}>
          {modalConfig.component}
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => handleClose()}
            style={{backgroundColor: 'white'}}>
            <View
              style={{
                padding: 12,
                marginHorizontal: 16,
                borderRadius: 50,
                backgroundColor: '#3580d8',
                alignItems: 'center',
                justifyContent: 'center',
                marginBottom: 70,
              }}>
              <Text style={{color: '#fff'}}>Done</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default ButtonGroup;
