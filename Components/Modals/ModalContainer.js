import React from 'react';
import {FullScreenModal, PartialScreenModal} from '.';

const ModalContainer = ({modalConfig}) => {
  if (modalConfig.isFullScreen) {
    return <FullScreenModal modalConfig={modalConfig} />;
  } else {
    return <PartialScreenModal modalConfig={modalConfig} />;
  }
};

export default ModalContainer;
