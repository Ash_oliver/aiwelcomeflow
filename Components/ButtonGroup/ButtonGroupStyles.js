import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  group: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  right: {
    justifyContent: 'flex-end',
  },
  spaceBetween: {
    justifyContent: 'space-between',
  },
});
