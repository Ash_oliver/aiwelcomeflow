import styles from './ButtonGroupStyles';
import React from 'react';
import {View} from 'react-native';

const ButtonGroup = ({type, children}) => {
  return <View style={[styles.group, styles[type]]}>{children}</View>;
};

export default ButtonGroup;
