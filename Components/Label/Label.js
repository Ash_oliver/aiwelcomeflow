import styles from './LabelStyles';
import React from 'react';
import {Text} from 'react-native';

const Label = ({text, isLarge, hasSpacing, isRequired}) => {
  const spacingStyle = hasSpacing ? styles.spacing : {};
  const largeStyle = isLarge ? styles.large : {};
  return (
    <Text style={[styles.label, largeStyle, spacingStyle]}>
      {text}
      {isRequired ? '*' : ''}
    </Text>
  );
};

export default Label;
