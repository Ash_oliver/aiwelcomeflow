import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  label: {
    color: '#383838',
    fontSize: 14,
    fontWeight: '600',
    marginBottom: 4,
  },
  large: {
    fontSize: 18,
  },
  spacing: {
    marginBottom: 20,
  },
});
