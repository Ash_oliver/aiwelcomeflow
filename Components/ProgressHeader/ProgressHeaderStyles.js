import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 16,
  },
  title: {
    fontSize: 20,
    fontWeight: '300',
    marginBottom: 16,
  },
  strong: {
    fontWeight: '400',
  },
});
