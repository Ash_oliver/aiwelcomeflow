import styles from './ProgressHeaderStyles';
import React from 'react';
import {View, Text} from 'react-native';
import {Sparkline} from './../index';

const ProgressHeader = ({title, step, maxStep}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        <Text style={[styles.title, styles.strong]}>
          Step {step} of {maxStep}:
        </Text>{' '}
        {title}
      </Text>
      <Sparkline isSlim={true} step={step} maxStep={maxStep} />
    </View>
  );
};

export default ProgressHeader;
