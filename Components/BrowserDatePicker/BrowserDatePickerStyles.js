import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  lastChild: {
    borderRightWidth: 1,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
  },
  firstChild: {
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0,
  },
  stripBorders: {
    borderRadius: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
  },
});
