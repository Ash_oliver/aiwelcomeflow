import styles from './BrowserDatePickerStyles';
import React from 'react';
import {View} from 'react-native';
import {days, years, months} from '../../Utils';
import {Select, Label} from '../index';

const data = [days, months, years];

const BrowserDatePicker = ({
  label,
  onClick,
  items,
  isLabelLarge,
  hasSpacing,
}) => {
  const lastChild = items.length - 1;

  return (
    <View>
      <Label text={label} isLarge={isLabelLarge} hasSpacing={hasSpacing} />
      <View style={styles.container}>
        {items.map((item, index) => (
          <Select
            key={index}
            onClick={onClick}
            onChange={item.onChange}
            options={data[index]}
            selectedValue={item.value}
            selectedLabel={item.value}
            style={[
              index !== 0 && index !== lastChild ? styles.stripBorders : {},
              lastChild === index ? styles.lastChild : {},
              index === 0 ? styles.firstChild : {},
            ]}
          />
        ))}
      </View>
    </View>
  );
};

export default BrowserDatePicker;
