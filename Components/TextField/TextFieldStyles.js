import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    marginBottom: 32,
  },
  field: {
    borderColor: '#b0b0b0',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 3,
    color: '#383838',
    paddingHorizontal: 12,
    paddingTop: 6,
    paddingBottom: 6,
    fontSize: 14,
    height: 44,
  },
});
