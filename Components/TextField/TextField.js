import styles from './TextFieldStyles';
import React, {useState, useEffect} from 'react';
import {TextInput, View, Text} from 'react-native';
import {Label} from './../index';

const TextField = ({
  label = '',
  value,
  onChange,
  isLabelLarge,
  hasSpacing,
  type = 'default',
  name,
  errorText,
  isErrorVisible,
  isRequired,
}) => {
  const [textValue, setTextValue] = useState(value);

  useEffect(() => {
    setTextValue(value);
  }, [value]);

  const handleChange = textValue => {
    onChange(name, textValue);
  };

  return (
    <View style={styles.container}>
      {label !== '' ? (
        <Label
          text={label}
          isLarge={isLabelLarge}
          hasSpacing={hasSpacing}
          isRequired={isRequired}
        />
      ) : null}
      <TextInput
        keyboardType={type}
        style={styles.field}
        onEndEditing={() => handleChange(textValue)}
        onChangeText={text => setTextValue(text)}
        value={textValue}
      />
      {isErrorVisible && <Text style={{color: 'red'}}>{errorText}</Text>}
    </View>
  );
};

export default TextField;
