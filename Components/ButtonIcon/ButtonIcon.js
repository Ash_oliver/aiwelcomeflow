import styles from './ButtonIconStyles';
import React from 'react';
import {TouchableOpacity} from 'react-native';
import Svg from './../../Svg';

const Button = ({icon, style, onPress}) => {
  const handlePress = () => {
    onPress();
  };
  return (
    <TouchableOpacity onPress={() => handlePress()}>
      <Svg icon="close" style={style} />
    </TouchableOpacity>
  );
};

export default Button;
