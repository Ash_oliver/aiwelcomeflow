import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  large: {
    height: 48,
    width: 48,
  },
});
