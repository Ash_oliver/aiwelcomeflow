import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    shadowColor: '#b0b0b0',
    borderRadius: 3,
    shadowOffset: {width: 0, height: 1},
    shadowRadius: 2,
    shadowOpacity: 0.8,
    elevation: 1,
    paddingVertical: 32,
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  icon: {
    width: 80,
    height: 80,
    marginBottom: 16,
    color: '#455d6e',
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    marginBottom: 8,
    color: '#383838',
  },
  description: {
    fontSize: 13,
    color: '#808080',
    marginBottom: 8,
  },
  textContainer: {
    flex: 1,
    width: '100%',
  },
});
