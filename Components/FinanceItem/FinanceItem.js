import styles from './FinanceItemStyles';
import React from 'react';
import {View, Text} from 'react-native';
import {TextField} from './../index';
import Svg from './../../Svg';

const FinanceItem = ({
  icon,
  title,
  description = '',
  textFields,
  onChange,
  data,
  validationErrors,
}) => {
  return (
    <View style={styles.container}>
      <Svg icon={icon} style={styles.icon} />
      <Text style={styles.title}>{title}</Text>
      {description != '' && (
        <Text style={styles.description}>{description}</Text>
      )}
      {textFields &&
        textFields.map((field, index) => (
          <View style={styles.textContainer} key={index}>
            <TextField
              name={field.name}
              label={field.label}
              value={data[field.name]}
              onChange={onChange}
              type="number-pad"
              errorText={field.errorText}
              isErrorVisible={validationErrors.includes(field.name)}
            />
          </View>
        ))}
    </View>
  );
};

export default FinanceItem;
