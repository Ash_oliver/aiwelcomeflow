import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 32,
    borderColor: '#b0b0b0',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 3,
  },
  element: {
    paddingHorizontal: 8,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    height: 42,
    minWidth: 60,
  },
  icon: {
    marginLeft: 8,
    width: 16,
    height: 16,
    color: 'black',
  },
});
