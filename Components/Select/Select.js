import styles from './SelectStyles';
import React, {useState} from 'react';
import {View, Text, TouchableOpacity, Picker} from 'react-native';
import Svg from '../../Svg';
import {Label} from './../index';

const PickerGroup = ({options, onChange, selectedValue}) => {
  const [selectedOption, setSelectedOption] = useState(selectedValue);
  const setValue = value => {
    onChange(value);
    setSelectedOption(value);
  };
  return (
    <View
      style={{
        backgroundColor: 'white',
      }}>
      <Picker
        selectedValue={selectedOption}
        onValueChange={(itemValue, itemIndex) => setValue(itemValue)}>
        {options.map(item => (
          <Picker.Item key={item.value} label={item.label} value={item.value} />
        ))}
      </Picker>
    </View>
  );
};

const Select = ({
  label = '',
  onClick,
  onChange,
  options,
  selectedValue,
  selectedLabel,
  style,
  hasSpacing,
  isLabelLarge,
}) => {
  const triggerModal = component => {
    onClick(component);
  };
  console.log(style);
  return (
    <View>
      {label !== '' ? (
        <Label text={label} isLarge={isLabelLarge} hasSpacing={hasSpacing} />
      ) : null}
      <View style={[styles.container, style]}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.element}
          onPress={() =>
            triggerModal(
              <PickerGroup
                options={options}
                onChange={onChange}
                selectedValue={selectedValue}
              />,
            )
          }>
          <Text>{selectedLabel}</Text>
          <Svg icon="caret" style={styles.icon} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Select;
