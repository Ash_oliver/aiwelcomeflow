import styles from './DatePickerStyles';
import React, {useState} from 'react';
import {View, Text, TouchableOpacity, DatePickerIOS} from 'react-native';
import Svg from '../../Svg';
import {Label} from '../index';
import {formatDate} from '../../Utils';

const PickerGroup = ({options, onChange, selectedValue, name}) => {
  const [selectedOption, setSelectedOption] = useState(selectedValue);
  const setValue = newDate => {
    onChange(name, newDate);
    setSelectedOption(newDate);
  };
  return (
    <View
      style={{
        backgroundColor: 'white',
      }}>
      <DatePickerIOS
        date={selectedOption}
        onDateChange={setValue}
        mode="date"
      />
    </View>
  );
};

const DatePicker = ({
  label = '',
  onClick,
  onChange,
  name,
  selectedValue,
  style,
  hasSpacing,
  isLabelLarge,
}) => {
  const triggerModal = component => {
    onClick({component: component, isFullScreen: false});
  };

  let value = selectedValue;
  if (typeof selectedValue === 'string') {
    value = new Date(selectedValue);
  }

  return (
    <View>
      {label !== '' ? (
        <Label text={label} isLarge={isLabelLarge} hasSpacing={hasSpacing} />
      ) : null}
      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={1}
          style={[styles.element, style]}
          onPress={() =>
            triggerModal(
              <PickerGroup
                onChange={onChange}
                name={name}
                selectedValue={value}
              />,
            )
          }>
          <Text>{formatDate(value, 'dd/mon/yyyy')}</Text>

          <Svg icon="caret" style={styles.icon} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DatePicker;
