import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginBottom: 32,
  },
  element: {
    paddingHorizontal: 8,
    borderColor: '#b0b0b0',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 3,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    height: 42,
    minWidth: 60,
  },
  lastChild: {
    borderRightWidth: 1,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
  },
  firstChild: {
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0,
  },
  stripBorders: {
    borderRadius: 0,
  },
  icon: {
    marginLeft: 8,
    width: 16,
    height: 16,
    color: 'black',
  },
});
