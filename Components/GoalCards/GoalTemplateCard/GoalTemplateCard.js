import styles from '../GoalCardStyles';
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {RadioButton} from '../index';
import Svg from './../../../Svg';

const GoalTemplateCard = ({goal, selectGoal}) => {
  const handleSelectGoal = id => {
    selectGoal(id);
  };

  return (
    <TouchableOpacity
      onPress={() => handleSelectGoal(goal.id)}
      style={styles.containerButtonTemplate}>
      <Svg icon={goal.icon} style={styles.goalIcon} />
      <View style={{flex: 1}}>
        <Text
          style={[
            styles.title,
            {
              marginBottom: 4,
            },
          ]}>
          {goal.name}
        </Text>
        <Text style={styles.description}>{goal.description}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default GoalTemplateCard;
