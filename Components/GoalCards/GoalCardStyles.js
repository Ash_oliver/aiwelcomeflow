import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  containerButtonEmpty: {
    flexDirection: 'column',
  },
  containerButtonTemplate: {
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 12,
    borderColor: '#b0b0b0',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 3,
    marginBottom: 16,
  },
  containerButton: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 12,
    shadowColor: '#808080',
    borderRadius: 3,
    shadowOffset: {width: 0, height: 1},
    shadowRadius: 2,
    shadowOpacity: 0.8,
    elevation: 1,
    marginBottom: 16,
  },
  iconContainer: {
    color: '#fff',
    padding: 12,
    borderRadius: 50,
    backgroundColor: '#3580d8',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 16,
  },
  goalIcon: {
    height: 48,
    width: 48,
    marginBottom: 20,
    marginRight: 16,
    color: '#455d6e',
  },
  detailsContainer: {
    justifyContent: 'space-between',
    flex: 1,
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontWeight: '600',
    fontSize: 18,
    color: '#383838',
  },
  description: {
    fontSize: 16,
    fontWeight: '300',
    color: '#383838',
  },
  spacing: {
    marginBottom: 16,
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footerItem: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerText: {
    fontWeight: '600',
    fontSize: 12,
    color: '#383838',
  },
  footerIcon: {
    color: '#c8cfd5',
    marginRight: 4,
  },
});
