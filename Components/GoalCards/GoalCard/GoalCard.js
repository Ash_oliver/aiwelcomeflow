import styles from '../GoalCardStyles';
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Svg from './../../../Svg';
import {GoalTemplateCard} from './../../index';
import {ChooseGoalScreen, GoalDetailsScreen} from './../../../Containers';

const GoalCard = ({isEmpty, goal, navigation, modalConfig}) => {
  const handleTrigger = (component, title) => {
    modalConfig.openModal({
      component: component,
      title: title,
      isFullScreen: true,
      isLoading: true,
    });
  };

  const FooterItem = ({icon, value}) => {
    return (
      <View style={styles.footerItem}>
        <Svg icon={icon} style={styles.footerIcon} />
        <Text style={styles.footerText}>{value}</Text>
      </View>
    );
  };

  if (isEmpty) {
    return (
      <TouchableOpacity
        onPress={() =>
          handleTrigger(
            <ChooseGoalScreen
              navigation={navigation}
              modalConfig={modalConfig}
            />,
            'Choose goal type',
          )
        }
        style={[styles.containerButton, styles.containerButtonEmpty]}>
        <View style={styles.iconContainer}>
          <Svg
            icon="add"
            style={{
              color: '#fff',
            }}
          />
        </View>
        <Text style={styles.title}>Add a new goal</Text>
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity
        onPress={() =>
          handleTrigger(
            <GoalDetailsScreen
              goal={goal}
              navigation={navigation}
              closeModal={modalConfig.closeModal}
            />,
            'Choose goal type',
          )
        }
        style={styles.containerButton}>
        <Svg icon={goal.icon} style={styles.goalIcon} />
        <View style={styles.detailsContainer}>
          <View style={styles.innerContainer}>
            <View style={styles.spacing}>
              <Text style={styles.title}>{goal.name}</Text>
              <Text style={styles.description}>{goal.estimatedValue}</Text>
            </View>
            <Svg icon="edit" />
          </View>
          <View style={styles.footerContainer}>
            <FooterItem icon="calendar" value={goal.yearEnd} />
            <FooterItem icon="star" value={goal.importance} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
};

export default GoalCard;
