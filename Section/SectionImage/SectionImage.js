import styles from '../SectionStyles';
import React from 'react';
import {Text, ImageBackground} from 'react-native';
import Images from './../../Images';
import {SlideUp} from './../../Transitions';

const SectionImage = ({text, image}) => {
  return (
    <ImageBackground
      source={Images[image]}
      resizeMode="cover"
      imageStyle={{height: 400}}
      style={styles.sectionImage}>
      <SlideUp>
        <Text style={styles.sectionTitle}>{text}</Text>
      </SlideUp>
    </ImageBackground>
  );
};

export default SectionImage;
