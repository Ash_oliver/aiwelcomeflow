import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  sectionDescription: {
    fontSize: 18,
    fontWeight: '300',
    color: '#383838',
    textAlign: 'center',
    lineHeight: 30,
  },
  sectionDescriptionLight: {
    color: '#808080',
  },
  sectionImage: {
    alignItems: 'center',
    height: 400,
    justifyContent: 'center',
  },
  section: {
    paddingLeft: 16,
    paddingRight: 16,
    paddingBottom: 40,
    paddingTop: 40,
  },
  light: {
    backgroundColor: '#fff',
  },
  dark: {
    backgroundColor: '#f8f8f8',
  },
  sectionTitle: {
    color: 'white',
    fontSize: 40,
    fontWeight: '300',
    textAlign: 'center',
  },
  sectionTitleDark: {
    marginBottom: 40,
    color: '#383838',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: '300',
  },
});
