import styles from '../SectionStyles';
import React from 'react';
import {Text} from 'react-native';

const SectionDescription = ({children, isLight}) => {
  const lightStyles = isLight ? styles.sectionDescriptionLight : {};
  return (
    <Text style={[styles.sectionDescription, lightStyles]}>{children}</Text>
  );
};

export default SectionDescription;
