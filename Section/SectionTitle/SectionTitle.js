import styles from '../SectionStyles';
import React from 'react';
import {Text} from 'react-native';

const SectionTitle = ({type, children}) => {
  const textType = type === 'dark' ? 'sectionTitleDark' : 'sectionTitle';
  return <Text style={styles[textType]}>{children}</Text>;
};

export default SectionTitle;
