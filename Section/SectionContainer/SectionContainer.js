import styles from '../SectionStyles';
import React from 'react';
import {View} from 'react-native';

const SectionContainer = ({children, theme = 'light'}) => {
  return <View style={[styles.section, styles[theme]]}>{children}</View>;
};

export default SectionContainer;
